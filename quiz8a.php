<?php
session_start();
include("database.php");
extract($_POST);
extract($_GET);
extract($_SESSION);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Assignment1</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="quiz.css" rel="stylesheet" type="text/css">
</head>
<body>

<?php
include("header.php");
$ClickMsg = $_POST["Enroll"]; 
echo '<table cellpadding="0" cellspacing="0" class="db-table">';
echo '<table width="100%" left="50" border="0" align="center">';
echo '<tr><th>College Name</th><th>Enrollment Total</th><th>Liability Total</th><th>NetAsset Total</th><th>Revenue Total</th><th> Revenue Per Student </th><th> Asset Per Student </th><th> Liabilities Per Student </th></tr>'; 
echo '<tr><th>            </th><th> </th><th>  </th></tr>';

function COALESCE() {
  $args = func_get_args();
  foreach ($args as $arg) {
    if (!empty($arg)) {
      return $arg;
    }
  }
  return NULL;
}

$BaseSQL = 'SELECT A.INSTNM, COALESCE(enrollment.EFYTOTLT,0) AS Enrollment, 
				  COALESCE(financials.F1A13,0) as Liability, 
				  COALESCE(financials.F1A18,0) as NetAsset,
				  COALESCE(financials.F1B25,0) AS Revenues, 
				  COALESCE(ROUND((financials.F1B01)/(enrollment.EFYTOTLT),2),0) AS Revenue_Per_Student, 
				  COALESCE(ROUND((financials.F1A18)/(enrollment.EFYTOTLT),2),0) as Asset_Per_Student,
				COALESCE(ROUND((financials.F1A13)/(enrollment.EFYTOTLT),2),0) as Liabilities_Per_Student
		FROM ';

$WhereSQL = ' LEFT OUTER JOIN financials 
    ON A.UNITID = financials.UNITID
    AND financials.`YEAR` = "2011"
    LEFT OUTER JOIN enrollment
 	ON A.UNITID= enrollment.UNITID 
    AND enrollment.EFFYLEV=1 AND enrollment.`YEAR` = "2011"';


switch ($ClickMsg) {
    case "Enrollment Total":
        $TempSQL='  (SELECT colleges.UNITID,colleges.INSTNM, enrollment.EFYTOTLT   FROM  enrollment INNER JOIN colleges 
					ON colleges.UNITID=enrollment.UNITID
					WHERE enrollment.EFFYLEV=1 AND enrollment.`YEAR` = "2011"
					ORDER BY enrollment.EFYTOTLT DESC LIMIT 20 ) AS A ';
		$OrderSQL='ORDER BY Enrollment DESC';			
        break;
	case "Liability Total":
	    $TempSQL='  (SELECT colleges.UNITID,colleges.INSTNM, financials.F1A13 
         FROM financials INNER JOIN colleges 
 	    ON colleges.UNITID=financials.UNITID and financials.`YEAR` = "2011"
        ORDER BY financials.F1A13 DESC LIMIT 20  ) AS A ';
        $OrderSQL=' ORDER BY Liability DESC LIMIT 20';
        break;
   case "NetAsset Total":
   $TempSQL='  (SELECT colleges.UNITID,colleges.INSTNM, financials.F1A18
				FROM financials INNER JOIN colleges 
			ON colleges.UNITID=financials.UNITID and financials.`YEAR` = "2011"
			ORDER BY financials.F1A18 DESC LIMIT 20 ) AS A ';
        $OrderSQL=' ORDER BY NetAsset DESC LIMIT 20';
        break;
   case "Revenue Total":
      $TempSQL='  (SELECT colleges.UNITID,colleges.INSTNM, financials.F1B25 
            		FROM financials INNER JOIN colleges 
            			ON colleges.UNITID=financials.UNITID AND financials.`YEAR` = "2011"
				ORDER BY financials.F1B25 DESC LIMIT 20 ) AS A ';
           $OrderSQL=' ORDER BY Revenues DESC LIMIT 20';
        break;		
	case "Revenue Per Student":
	    $TempSQL='  (SELECT colleges.UNITID,colleges.INSTNM,ROUND((financials.F1B01)/(enrollment.EFYTOTLT),2) AS "Student_Revenue"             
					FROM financials 
					JOIN colleges
					ON colleges.UNITID=financials.UNITID
					JOIN enrollment
					ON enrollment.UNITID=colleges.UNITID and financials.`YEAR` = "2011"
					AND enrollment.`YEAR` = financials.`YEAR`
					WHERE enrollment.EFFYLEV=1  and financials.`YEAR` = "2011"
					ORDER BY Student_Revenue DESC LIMIT 20 ) AS A ';
        $OrderSQL=' ORDER BY Revenue_Per_Student DESC LIMIT 20';
        break;		
	case "Asset Per Student":
	   $TempSQL='  (SELECT colleges.UNITID,colleges.INSTNM,ROUND((financials.F1A18)/(enrollment.EFYTOTLT),2) AS "Student_Assets"             
					FROM financials 
					JOIN colleges
					ON colleges.UNITID=financials.UNITID
					JOIN enrollment
					ON enrollment.UNITID=colleges.UNITID and financials.`YEAR` = "2011"
					AND enrollment.`YEAR` = financials.`YEAR`
					WHERE enrollment.EFFYLEV=1  and financials.`YEAR` = "2011"
					ORDER BY Student_Assets DESC LIMIT 20 ) AS A ';
       $OrderSQL=' ORDER BY Asset_Per_Student DESC LIMIT 20';
        break;		
	case "Liabilities Per Student":
	   $TempSQL='  (SELECT colleges.UNITID,colleges.INSTNM,ROUND((financials.F1A13)/(enrollment.EFYTOTLT),2) AS "Student_Liabilities"             
					FROM financials 
					JOIN colleges
					ON colleges.UNITID=financials.UNITID
					JOIN enrollment
					ON enrollment.UNITID=colleges.UNITID and financials.`YEAR` = "2011"
					AND enrollment.`YEAR` = financials.`YEAR`
					WHERE enrollment.EFFYLEV=1  and financials.`YEAR` = "2011"
					ORDER BY Student_Liabilities DESC LIMIT 20 ) AS A ';
        $OrderSQL=' ORDER BY Liabilities_Per_Student DESC LIMIT 20';
        break;		
}
$Query=$BaseSQL	. $TempSQL . $WhereSQL . $OrderSQL;
$rs=mysql_query($Query,$cn) or die(mysql_error());
	   while($row = mysql_fetch_assoc($rs)) {
	            echo '<tr>';
             	$INSTNM = $row['INSTNM'];
				$Student_Liabilities = $row['Liabilities_Per_Student'];
					echo '<td>',$INSTNM,'</td>';
				echo '<td>',number_format($row['Enrollment']),'</td>';
				echo '<td>',number_format($row['Liability']),'</td>';
				echo '<td>',number_format($row['NetAsset']),'</td>';
				echo '<td>',number_format($row['Revenues']),'</td>';
				echo '<td>',number_format($row['Revenue_Per_Student']),'</td>';
				echo '<td>',number_format($row['Asset_Per_Student']),'</td>';
				echo '<td>',number_format($Student_Liabilities),'</td>';
				echo '</tr>';
							
			}
?>


</body>
</html>